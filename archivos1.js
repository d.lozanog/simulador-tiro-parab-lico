function dibujarT(){
    let lienzo= document.getElementById("lienzo");
    let ctx=lienzo.getContext('2d');
    lienzo.width=500;
    lienzo.height=300;
    let vo= document.getElementById("vo");
    let wo= document.getElementById("wo");
    ctx.save();
ctx.beginPath();

 ctx.globalAlpha = 0.0;
ctx.fillRect(10,250,50,-30);

ctx.translate(10,228);

ctx.rotate((Math.PI / 180) * (360- wo.value));

ctx.translate(10,-228);
ctx.globalAlpha = 1.0;
ctx.fillStyle = 'red';
ctx.fillRect(1,250,50,-30);
ctx.restore();    
  
    
    
    let t=0; 
    let x=10;
    let y=240;
    let xo=x, yo=y;
    radius=5;

    while(t<=5000){
        x= xo + (vo.value*(Math.cos((Math.PI/180)*(360-wo.value)))*t);
        y= yo + (vo.value*(Math.sin((Math.PI/180)*(360-wo.value)))*t)+((9.8*(t*t))/2);
      ctx.beginPath();
        ctx.arc(x, y, radius, 0, Math.PI * 2, true);
        ctx.closePath();
        ctx.fillStyle = 'blue';

        ctx.fill();

        t=t+0.5;
    }

}